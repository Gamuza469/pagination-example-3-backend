const express = require('express');
const router = express.Router();

const CommentsController = require('../controllers/CommentsController');

router.get('/', CommentsController.findAll);

module.exports = router;
