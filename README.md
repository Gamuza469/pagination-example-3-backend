# Express Basic Server

Basic implementation of a back-end using Express.js. Use this as a base to create example projects or code.

## Usage

Install dependencies using `npm install` then `npm start` to run the back-end server. Once up, point your browser to `http://localhost:8080`.

## Configuration

You can change the listening port at the `.env` file.
