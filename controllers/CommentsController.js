const { database } = require('../database');

/*
const findAll = async function (request, response) {
    const results = await database('comments').select();

    console.info('Requested all comments.');

    response.json(results);
};
*/

const findAll = async function (request, response) {
    /* Parameters (from client side) */
    const { pageToGet, commentsPerPage } = request.query;

    if ( !pageToGet || !commentsPerPage ) {
        response
            .status(500)
            .json({ message: 'The page to get (pageToGet) and/or the comments per page (commentsPerPage) arguments are not set!' });

        console.error('Petition was made without the required parameters.');

        return;
    }
     
    /* Get total comment count */
    const [ countResult ] = await database('comments').count({ totalCount: '*' });
    const { totalCount } = countResult;

    let availablePages = Math.trunc(totalCount / commentsPerPage);

    /* Add extra page if needed */
    if ( totalCount % commentsPerPage ) {
        availablePages++;
    }

    /* Get paged comments */
    const offset = commentsPerPage * ( pageToGet - 1 );
    const pagedComments = await database('comments').offset(offset).limit(commentsPerPage);

    //console.log(pagedComments);
    console.log('Total comment count: ', totalCount);
    console.log('Total pages to browse: ', availablePages);
    console.log('Comments per page: ', commentsPerPage);
    console.log('Current selected page: ', pageToGet);
    console.log('Selected page comment count: ', pagedComments.length);

    console.info('Requested paged comments.');

    const result = {
        comments: pagedComments,
        count: totalCount,
        pages: availablePages,
        selectedPage: pageToGet,
        elementsPerPage: commentsPerPage
    };

    response.json(result);
};


module.exports = {
    findAll
};
