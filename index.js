require("dotenv").config();
const { PORT } = process.env;

const express = require('express');
const cors = require('cors');
const app = express();
const rootRouter = require('./routes');

const { initDb } = require('./database');
const getComments = require('./database/getComments');

const execute = async () => {
    /* Exercise setup */
    const database = await initDb();
    const allComments = await getComments();

    await database('comments').insert(allComments);

    app.use(cors());

    
    app.use('/', rootRouter);

    app.listen(
        PORT,
        () => console.info(`Back-end server listening at port ${PORT}.`)
    );
};

execute();
