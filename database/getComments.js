const axios = require('axios');
const fs = require('fs');
const path = require('path');

const commentsJsonPath = path.join(__dirname, '../data/comments.json');

const getComments = async () => {
    if (fs.existsSync(commentsJsonPath)) {
        console.info('Retrieving comments from local JSON file...');
        return getCommentsJson();
    }

    console.info('Getting comments from API...');

    const response = await axios.get('https://jsonplaceholder.typicode.com/comments');
    const comments = response.data;

    fs.writeFileSync(commentsJsonPath, JSON.stringify(comments));

    console.info('Comments saved to local JSON file.');

    return comments;
};

const getCommentsJson = () => {
    const commentsJson = fs.readFileSync(commentsJsonPath);
    const comments = JSON.parse(commentsJson);

    if (Array.isArray(comments) && !comments.length) {
        throw new Error('JSON array has length of zero.');
    }

    if (typeof comment === 'object' && !Object.keys(comment).length) {
        throw new Error('JSON object has no attributes.');
    }
    
    return comments;
};

module.exports = getComments;
